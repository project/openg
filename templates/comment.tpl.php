<div class="comment<?php print ($unpublished) ? ' unpublished' : ''; print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; if ($comment->uid == $node->uid) : print ' comment-node-author'; endif; ?> clearfix">
  <?php if ($title): ?>
    <h3 class="title">
      <?php print $title; ?>
      <?php if ($new): ?>
        <span class="new"><?php print $new; ?></span>
      <?php endif; ?>
    </h3>
  <?php elseif ($new): ?>
    <div class="new"><?php print $new; ?></div>
  <?php endif; ?>
  <?php print $picture; ?>
  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>
  <?php if ($submitted): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content; ?>
    <?php if ($signature): ?>
      <div class="user-signature clearfix">
        <?php print $signature; ?>
      </div>
    <?php endif; ?>
  </div>

  <?php print $links; ?>
</div> <!-- END .comment -->
