<?php if ($content_prefix && $page): ?>
<div id="content-prefix">
<?php print $content_prefix; ?>
</div>
<?php endif; ?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $node_classes; if (!$status) { print ' node-unpublished'; } ?>">

  <?php if ($status_flag): ?>
    <div class="node-status"><?php print $status_flag ?></div>
  <?php endif; ?>

  <?php if (($title) && $page): ?>
  <h1 id="page-title" class="title"><?php print $title ?></h1>
  <?php endif; ?>

  <?php if (($title) && !$page): ?>
  <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <?php print $picture ?>

  <?php if ($submitted): ?>
  <div class="post-info">
    <?php if ($submitted): ?>
    <div class="submitted">
    <?php print $submitted ?>
    </div>
    <?php endif; ?>
  </div>
  <?php endif; ?>
  <?php if ($terms): ?>
    <div class="terms">
    <?php print $terms ?>
    </div>
  <?php endif; ?>

  <div class="content">
  <?php print $content ?>
  </div>
  <?php if ($links): ?>
  <div class="post-links"><?php print $links; ?>
  </div>
  <?php endif; ?>
</div><!--#node-<?php print $node->nid; ?> ends here -->
<?php if ($content_suffix   && $page): ?>
<div id="content-suffix">
<?php print $content_suffix; ?>
</div>
<?php endif; ?>
