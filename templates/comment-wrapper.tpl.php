<?php
// $Id: comment-wrapper.tpl.php,v 1.2 2007/08/07 08:39:35 goba Exp $
?>
<div id="comments">
  <h2 class="comment-counter"><?php print $comment_title; ?></h2>
  <?php if($comments_message): ?>
  <div class="comment-message"><?php print $comments_message; ?></div>
  <?php endif; ?>
  <div class="ca"></div>
  <?php print $content; ?>
</div>
