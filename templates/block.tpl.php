<div id="<?php print $block_id; ?>" class="<?php print $block_classes ?>">
<?php if ($block->subject): ?>
<h3 class="title"><?php print $block->subject ?></h3>
<?php endif;?>
<div class="content">
<?php print $block->content ?>
</div>
</div><!--#<?php print $block_id; ?> ends here-->
