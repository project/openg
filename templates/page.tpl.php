<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if IE 7]>
    <?php print $ie7_styles; ?>
    <![endif]-->
    <!--[if lte IE 6]>
    <?php print $ie6_styles; ?>
    <![endif]-->
  </head>

  <body class="<?php print $body_classes; ?>">
    <div id="page-wrapper" class="<?php print $page_classes; ?>">
      <div id="inner-page-wrapper">

        <div id="header-wrapper">
          <div id="header-container" class="<?php print $header_classes; ?>">
            <div id="header" class="region <?php print $header_grid; ?>">
            <div id="skip-link">
              <a href="#main" class="element-invisible element-focusable"><?php print $skip_to_content; ?></a>
            </div>

              <?php if($header): ?>
              <div id="header-region">
              <?php print $header; ?>
              </div>
              <?php endif; ?>

              <?php if ($logo): ?>
                <?php print $logo; ?>
              <?php endif; ?>

              <?php if ($sitename): ?>
              <div class="site-name">
                <?php print $site_name; ?>
              </div>
              <?php endif;?>
               <?php if ($site_slogan): ?>
              <div class="site-slogan">
              <?php print $site_slogan;?>
              </div>
              <?php endif;?>

              <?php if($search_box): ?>
              <div class="search-box">
              <?php print $search_box; ?>
              </div>
              <?php endif; ?>

            </div><!--#header end-->
          </div><!--#header-container end-->
        </div><!--#header-wrapper end-->

        <?php if (($breadcrumb) || ($content_top)): ?>
        <div id="content-top-wrapper">
          <div id="content-top-container" class="<?php print $content_top_classes; ?>">
            <div id="content-top" class="region <?php print $content_top_grid; ?>">

            <?php if ($breadcrumb): ?>
            <div id="breadcrumbs">
            <?php print $breadcrumb; ?>
            </div>
            <?php endif; ?>

            <?php print $content_top; ?>

            </div>
          </div>
        </div><!--#content-top-wrapper end-->
        <?php endif;?>

        <div id="content-wrapper">
          <div id="content-container" class="<?php print $content_classes; ?>">
            <div id="content" class="region <?php print $content_grid; ?>">

              <div id="main" class="region <?php print $main_grid; ?><?php print $with_tabs; ?>">

              <?php if ($mission): ?>
              <div id="mission">
              <?php print $mission; ?>
              </div><!-- #mission end-->
              <?php endif; ?>

              <div class="content-output">

              <?php if ($content_prefix && !$page && !$node): ?>
              <div id="content-prefix">
              <?php print $content_prefix; ?>
              </div>
              <?php endif; ?>

              <?php if (($title) && (!$node || ($node && arg(2) !=''))): ?>
              <h1 class="title" id="page-title" ><?php print $title; ?></h1>
              <?php endif; ?>

              <?php if ($tabs2): ?>
              <ul class="tabs secondary">
              <?php print $tabs2 ?>
              </ul>
              <?php endif; ?>

              <?php if (($messages)): print $messages; endif; ?>
              <?php if (($help)): print $help; endif; ?>

              <?php print $content; ?>

              <?php if ($content_suffix && !$page && !$node): ?>
              <div id="content-suffix">
              <?php print $content_suffix; ?>
              </div>
              <?php endif; ?>

              </div>

              <?php if ($tabs): ?>
              <div id="tabs-wrapper">
              <?php endif; ?>
              <?php if ($tabs): ?>
              <div class="tabs">
              <?php print $tabs; ?>
              </div>
              </div>
              <?php endif; ?>

              </div><!-- #main end-->

              <?php if ($sidebar_first): ?>
              <div id="sidebar-first" class="region <?php print $sidebar_first_grid; ?>">
              <?php print $sidebar_first; ?>
              </div><!-- #sidebar-first end-->
              <?php endif; ?>

              <?php if ($sidebar_second): ?>
              <div id="sidebar-second" class="region <?php print $sidebar_second_grid; ?>">
              <?php print $sidebar_second; ?>
              </div><!-- #sidebar-second end -->
              <?php endif; ?>

            </div><!--#content end-->
          </div><!--#content-container end-->
        </div><!-- #content-wrapper end-->

        <?php if ($content_bottom): ?>
        <div id="content-bottom-wrapper">
          <div id="content-bottom-container" class="<?php print $content_bottom_classes; ?>">
            <div id="content-bottom" class="region <?php print $content_bottom_grid; ?>">
            <?php print $content_bottom; ?>
            <div class="ca"></div>
            </div>
          </div>
        </div><!--#content-bottom-wrapper end -->
        <?php endif;?>

        <?php if($primary_links): ?>
        <div id="main-navigation-wrapper">
        <div id="main-navigation" class="<?php print $main_navigation_classes?>">
        <div class="primary-links">
        <?php print $primary_links; ?>
        </div>
        <div class="secondary-links">
        <?php print $secondary_links; ?>
        </div>
        </div>
        </div><!--#main-navigation-wrapper end-->
        <?php endif;?>

        <div id="footer-wrapper">
          <div id="footer-container" class="<?php print $footer_classes; ?>">
            <div id="footer" class="region <?php print $footer_grid; ?>">

              <?php if ($footer): ?>
              <div id="footer-content"><?php print $footer; ?>
              </div>
              <?php endif; ?>
              <?php if ($footer_message): ?>
              <div class="footer-message"><?php print $footer_message; ?>
              </div>
              <?php endif; ?>
              <?php if($copyright): ?>
                <div class="copyright"><?php print $copyright; ?></div>
              <?php endif; ?>
              <?php print $feed_icons ?>
              <?php if($base_text): ?>
              <div class="base-text"><?php print $base_text; ?></div>
              <?php endif; ?>

            </div>
          </div><!--#footer-container end -->
        </div><!--#footer-wrapper end -->

      </div><!-- #inner-page end -->

    <?php if($scroll_to_top): ?>
    <div class="scroll-to-top"><a title="<?php print $scroll_to_top; ?>" href="#header"><?php print $scroll_to_top; ?></a>
    </div>
    <?php endif; ?>

    <?php if ($tray_region): ?>
    <div class="tray-close">Tray
    </div>
    <div id="tray-wrapper">
    <?php print $tray_region; ?>
    </div>
    <?php endif; ?>

    <?php print $scripts; ?>
    <?php print $closure; ?>

    </div><!-- #page-wrapper end -->
</body>
</html>
