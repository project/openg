<?php


/**
 * Theme setting defaults
 */
function opengrid_default_theme_settings() {
  $defaults = array(
    'css_switch' => 0,
    'opengrid_js_control'=> 0,
    'comments_wrapper_title' => 'Comment',
    'comments_empty_text' => 'Start the conversation ',
    'comments_string' => 'Join the conversation',
    'comments_subject' => 1,
    'comments_user_name' => 1,
    'sf_hi' => 1,
    'sf_dropshadows' => 1,
    'sf_delay' => 1200,
    'sf_speed' => 'normal',
    'sf_subs_min_width' => 12,
    'sf_subs_max_width' => 27,
    'sf_subs_centralized' => 0,
    'hide_sb_paths' => 'admin/*
node/add/*
node/*/edit
sitemap',
  );

  // Add site-wide theme settings
  $defaults = array_merge($defaults, theme_get_settings());

  return $defaults;
}

/**
 * Initialize theme settings if needed
 */
function opengrid_initialize_theme_settings($theme_name) {
  $theme_settings = theme_get_settings($theme_name);
  if (!isset($theme_settings['css_switch']) || $theme_settings['opengrid_rebuild_registry'] == 1) {
    static $registry_rebuilt = false;   // avoid multiple rebuilds per page

    // Rebuild theme registry & notify user
    if(isset($theme_settings['opengrid_rebuild_registry']) && $theme_settings['opengrid_rebuild_registry'] == 1 && !$registry_rebuilt) {
      drupal_rebuild_theme_registry();
      drupal_set_message(t('Theme registry rebuild completed. <a href="!link">Turn off</a> this feature for production websites.', array('!link' => url('admin/build/themes/settings/' . $GLOBALS['theme']))), 'warning');
      $registry_rebuilt = true;
    }

    // Retrieve saved or site-wide theme settings
    $theme_setting_name = str_replace('/', '_', 'theme_'. $theme_name .'_settings');
    $settings = (variable_get($theme_setting_name, FALSE)) ? theme_get_settings($theme_name) : theme_get_settings();

    // Skip toggle_node_info_ settings
    if (module_exists('node')) {
      foreach (node_get_types() as $type => $name) {
        unset($settings['toggle_node_info_'. $type]);
      }
    }

    // Combine default theme settings from .info file & theme-settings.php
    $theme_data = list_themes();   // get theme data for all themes
    $info_theme_settings = ($theme_name) ? $theme_data[$theme_name]->info['settings'] : array();
    $defaults = array_merge(opengrid_default_theme_settings(), $info_theme_settings);

    // Set combined default & saved theme settings
    variable_set($theme_setting_name, array_merge($defaults, $settings));

    // Force theme settings refresh
    theme_get_setting('', TRUE);
  }
}

/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function opengrid_settings($saved_settings, $subtheme_defaults = array()) {

  global $base_url;

  // Get theme name from url (admin/.../theme_name)
  $theme_name = arg(count(arg()) - 1);

  // Check for a new uploaded sitename img, and use that instead.
  if ($file = file_save_upload('sitename_upload', array('file_validate_is_image' => array()))) {
    $parts = pathinfo($file->filename);
    $filename = str_replace('/', '_', $theme_name) . '_sitename.' . $parts['extension'];
    drupal_set_message(t('Background Image for Site Name was updated.'));
    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      $_POST['sitename_path'] = $file->filepath;
    }
  }

  // Combine default theme settings from .info file & theme-settings.php
  $theme_data = list_themes();   // get data for all themes
  $info_theme_settings = ($theme_name) ? $theme_data[$theme_name]->info['settings'] : array();
  $defaults = array_merge(opengrid_default_theme_settings(), $info_theme_settings);

  // Allow a subtheme to override the default values.
  $defaults = array_merge($defaults, $subtheme_defaults);

  // Combine default and saved theme settings
  $settings = array_merge($defaults, $saved_settings);

  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();

  //Background image support for Sitename
  $form['sitename_bg'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sitename image settings'),
      '#description' => t('The following image will be displayed as a backgorund for the sitename. This can be useful in situations where the sitename has to be in a special font and only an image is a suitable option.'),
      '#collapsible' => true,
      '#collapsed' => true,
  );
  $form['sitename_bg']['sitename_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to custom sitename image'),
      '#default_value' => $settings['sitename_path'],
      '#description' => t('The path to the file you would like to use as your sitename background image.'),
  );
  $form['sitename_bg']['sitename_alt'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt text for sitename image'),
      '#default_value' => $settings['sitename_alt'],
      '#description' => t('Defaults to the <em>Site name</em>.'),
  );
  $form['sitename_bg']['sitename_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title text for sitename image'),
      '#default_value' => $settings['sitename_title'],
  );
  $form['sitename_bg']['sitename_upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload sitename image'),
      '#maxlength' => 40,
      '#description' => t("If you don't have direct file access to the server, use this field to upload your sitename image."),
  );


  //Breadcrumbs


  $form['breadcrumb'] = array(
      '#type' => 'fieldset',
      '#title' => t('Breadcrumb settings'),
      '#collapsible' => true,
      '#collapsed' => true,
  );
  $form['breadcrumb']['show_breadcrumbs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display breadcrumbs'),
      '#default_value' => $settings['show_breadcrumbs'],
      '#attributes' => array('class' => 'settings_trigger'),
  );
  $form['breadcrumb']['breadcrumbs_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumbs Settings'),
    '#collapsible' => false,
    '#collapsed' => false,
    '#attributes' => array('class' => 'breadcrumbs-settings hidden-settings'),
  );
  $form['breadcrumb']['breadcrumbs_settings']['breadcrumb_separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator for breadcrumbs'),
      '#size' => 4,
      '#default_value' => $settings['breadcrumb_separator'],
      '#description' => t('Provide a character to seperate breadcrumps like: &raquo; Blank spaces count'),
  );
  $form['breadcrumb']['breadcrumbs_settings']['breadcrumb_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title for breadcrumbs'),
      '#size' => 60,
      '#description' => t('Add an introduction text for breadrumbs like: You are here: '),
      '#default_value' => $settings['breadcrumb_title'],
  );


  //External Fonts

  $form['external_font'] = array(
    '#type' => 'fieldset',
    '#title' => t('External Font'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Add the full url to an external font file. Eg: http://fonts.googleapis.com/css?family=Copse'),
  );
  $form['external_font']['font_file_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Full url to font file'),
    '#default_value' => $settings['font_file_url'],
    '#size' => 50,
  );


  //Copyrighth

  $form['copyright'] = array(
    '#type' => 'fieldset',
    '#title' => t('Copyright Info'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Show copyright info in the footer region. Automatically updates to current date. This saves you the schlep of having to change the date each year.'),
  );
  $form['copyright']['start'] = array(
    '#type' => 'textfield',
    '#title' => t('Year of start'),
    '#default_value' => $settings['start'],
    '#size' => 10,
  );
  $form['copyright']['copyright_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Copyrigth to'),
    '#default_value' => $settings['copyright_name'],
    '#size' => 30,
  );


  //Closing Footer Text

  $form['footer_base'] = array(
      '#type' => 'fieldset',
      '#title' => t('Closing Footer Text'),
      '#collapsible' => true,
      '#collapsed' => true,
      '#description' => t('content displayed at the bottom of the page'),
  );
  $form['footer_base']['base_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Content'),
      '#default_value' => $settings['base_text'],
  );

  //Main Navigation with Superfish

  $form['main_navigation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main Navigation dropdown menus with Superfish.'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['main_navigation']['dropdown_options'] = array(
    '#type' => 'radios',
    '#title' => t('Dropdowns Options'),
    '#default_value' => $settings['dropdown_options'],
    '#options' => array(
      t('Show Dropdowns Only'),
      t('Show Dropdowns and Contextual Submenus'),
      t('Show Contextual Submenus Only')
    ),
    '#attributes' => array('class' => 'superfish-options'),
  );
  $form['main_navigation']['navigation_extras'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra classes'),
    '#size' => 20,
    '#default_value' => $settings['navigation_extras'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#description' => t('A comma seperated list of classes. Use this classes for superfish custom styles.'),
  );
  $form['main_navigation']['superfish_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Superfish Settings'),
    '#collapsed' => false,
    '#collapsible' => false,
    '#attributes' => array('class' => 'superfish-settings hidden-settings'),
  );
  $form['main_navigation']['superfish_settings']['sf_hi'] = array (
    '#type' => 'checkbox',
    '#title' => t('Hover Intent Support'),
    '#default_value' => $settings['sf_hi'],
    '#description' => t('Usability enhancement by hoverIntent detection'),
  );
  $form['main_navigation']['superfish_settings']['sf_dropshadows'] = array (
    '#type' => 'checkbox',
    '#title' => t('Drop Shadows'),
    '#default_value' => $settings['sf_dropshadows'],
    '#description' => t('Completely disable drop shadows by unchecking this.'),
  );
  $form['main_navigation']['superfish_settings']['sf_subs_centralized'] = array (
    '#type' => 'checkbox',
    '#title' => t('Centralize Submenus'),
    '#default_value' => $settings['sf_subs_centralized'],
    '#description' => t('Centralize the Sub-menu in relation with the Parent Link.'),
  );
  $form['main_navigation']['superfish_settings']['sf_delay'] = array (
    '#type' => 'textfield',
    '#title' => t('Delay time'),
    '#size' => 4,
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#default_value' => $settings['sf_delay'],
    '#description' => t('The delay in milliseconds that the mouse can remain outside a submenu without it closing.'),
  );
  $form['main_navigation']['superfish_settings']['sf_speed'] = array (
    '#type' => 'select',
    '#title' => t('Speed'),
    '#options' => array(
      'slow' => t('Slow'),
      'normal' => t('Normal'),
      'fast' => t('Fast'),
    ),
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#default_value' => $settings['sf_speed'],
    '#description' => t('Speed of the animation.'),
  );
  $form['main_navigation']['superfish_settings']['sf_subs'] = array (
    '#type' => 'markup',
    '#value' => '<h3>'. t('Sub Links Widths.') .'</h3>',
  );
  $form['main_navigation']['superfish_settings']['sf_subs_min_width'] = array (
    '#type' => 'textfield',
    '#title' => t('Min Width'),
    '#size' => 2,
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#default_value' => $settings['sf_subs_min_width'],
    '#description' => t('Minimum width of sub-menus in em units.'),
  );
  $form['main_navigation']['superfish_settings']['sf_subs_max_width'] = array (
    '#type' => 'textfield',
    '#title' => t('Max Width'),
    '#size' => 2,
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#default_value' => $settings['sf_subs_max_width'],
    '#description' => t('Maximum width of sub-menus in em units.'),
  );


  //Comments Settings
  $form['og_comments'] = array(
    '#type' => 'fieldset',
    '#title' => t('OpenG Comments Settings.'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['og_comments']['comments_counter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Comments counter.'),
    '#default_value' => $settings['comments_counter'],
    '#description' => t('Toggle the visibility of default comment counter'),
  );
  $form['og_comments']['comments_subject'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Subject for Comments'),
    '#default_value' => $settings['comments_subject'],
    '#description' => t('Shows a linked excerpt of comment or comment subject above the comment'),
  );
  $form['og_comments']['comments_user_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove Not Verify String from User Names'),
    '#default_value' => $settings['comments_user_name'],
  );
  $form['og_comments']['comments_wrapper_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Comments Wrapper title.'),
    '#default_value' => $settings['comments_wrapper_title'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#description' => t('Use a singular word. Ex: comment, thread, conversation, habbahabba.'),
  );
  $form['og_comments']['comments_empty_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Comments Empty Text.'),
    '#default_value' => $settings['comments_empty_text'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#description' => t('Text to display when no comments are present eg: <em>Start the conversation.</em>'),
  );
  $form['og_comments']['comments_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Comments Anchor Text.'),
    '#default_value' => $settings['comments_string'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#description' => t('Text to display when comments are present eg: <em>Join the conversation</em>.'),
  );


  // Sidebars controls

  $form['sidebars_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sidebars Hide Controls'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['sidebars_fieldset']['hide_sidebars'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Toggle Sidebars Button.'),
    '#default_value' => $settings['hide_sidebars'],
    '#description'   => t('Provides a hover link to toggle sidebar visibility'),
  );
  $form['sidebars_fieldset']['hide_sb_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Hide sidebars in these paths:'),
    '#default_value' => $settings['hide_sb_paths'],
    '#description' => t('Write each path in a new line(&lt;front&gt;  for frontpage, use * as a wildcard).'),
  );


  // UI widgets

  $form['opengrid_ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('OpenG UI Widgets'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#description' => t('Usability modifications to the default Drupal UI'),
  );
  $form['opengrid_ui']['popup_messages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Messages in a Popup.'),
    '#default_value' => $settings['popup_messages'],
    '#description' => module_exists('jquery_ui') ? t('Show Drupal messages in a floating modal box.') : ('Sorry, Jquery UI module must be active.'),
    '#attributes' => array('class' => 'settings_trigger'),
    '#disabled' => module_exists('jquery_ui') ? false : true,
  );

  $form['opengrid_ui']['auto_hide_messages'] = array(
    '#type'          => module_exists('jquery_ui') ? 'checkbox' : 'hidden',
    '#title'         => t('Auto hide status messages.'),
    '#default_value' => $settings['auto_hide_messages'],
    '#description' => t('Messages will dissapear after 5 seconds'),
    '#prefix' => '<div class="hidden-settings">',
    '#suffix' => '</div>',
    '#disabled' => module_exists('jquery_ui') ? false : true,
  );

  $form['opengrid_ui']['scroll_to_top'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Scroll to top Button.'),
    '#default_value' => $settings['scroll_to_top'],
    '#description' => t('Provide a scroll to top button.'),
  );

  $form['opengrid_ui']['fixed_buttons'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Fixed Submit Buttons.'),
    '#default_value' => $settings['fixed_buttons'],
    '#description' => t('Set a fixed position for submit buttons on node forms (useful in long forms).'),
  );

  // Labelify

  $form['opengrid_ui']['labelify'] = array(
    '#type' => 'fieldset',
    '#title' => t('Compact Forms'),
    '#collapsible' => false,
    '#collapsed' => false,
    '#description' => t('Select forms to compact by moving labels into the form fields.'),
  );

    $form['opengrid_ui']['labelify']['labelify_ids'] = array(
      '#type' => 'select',
      '#title' => t(''),
      '#options' => array(
        'search_block_form' => t('Search Block Form'),
        'comment_form' => t('Comment Form'),
        'contact_mail_page' => t('Contact Form'),
        'user_login' => t('Login Form'),
        'user_register' => t('Registration form'),
        'user_pass' => t('Password Form'),
        'node_form' => t('Node Add Forms'),
      ),
      '#multiple' => true,
      '#default_value' => $settings['labelify_ids'],
    );


  // Dev widgets

  $form['opengrid_dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('OpenG Developer Widgets'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  $form['opengrid_dev']['opengrid_rebuild_registry'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Rebuild theme registry on every page.'),
    '#default_value' => $settings['opengrid_rebuild_registry'],
    '#description'   => t('During theme development, it can be useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
  );

  $form['opengrid_dev']['og_guides_bg'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show bg images.'),
    '#default_value' => $settings['og_guides_bg'],
    '#description'   => t('Adds a small background image to blocks and certain regions that helps with identification. Useful during theme development.'),
  );

  $form['opengrid_dev']['og_guides_borders'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show light grey borders and background colors that help understand the spatial arrangement of blocks and regions.'),
    '#default_value' => $settings['og_guides_borders'],
  );


/***************************************************************
****** Grid Layout Settings ************************************
***************************************************************/

  $form['opengrid_grids_title'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'. t('OpenG Grid Settings.') .'</h3>',
  );

  //containers settings form

  $form['containers_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Container Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => true,
    '#description' => t('Container defines the number of grid columns'),
    '#attributes' => array('class' => 'containers-settings'),
  );

  $form['containers_fieldset']['container_columns'] = array(
    '#type' => 'select',
    '#title' => t('Grid Settings'),
    '#attributes' => array('class' => 'input-inline'),
    '#options' => array(
  'none' => t('none'),
  'container_12' => t('12'),
  'container_16' => t('16'),
  'container_24' => t('24'),
    ),
    '#default_value' => $settings['container_columns'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
  );

  $form['containers_fieldset']['css_switch'] = array(
    '#type' => 'select',
    '#title' => t('Select CSS'),
    '#options' => array(t('fixed'), t('fluid')),
    '#default_value' => $settings['css_switch'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
  );

  $form['containers_fieldset']['container_extras'] = array(
    '#type' => 'textfield',
    '#title' => t('Container Extras'),
    '#attributes' => array('class' => 'input-inline'),
    '#size' => 20,
    '#default_value' => $settings['container_extras'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
  );

  $form['containers_fieldset']['independent_containers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Container by Region'),
    '#description' => t('Set independent containers for each region. Usefull for designs that require full screen width backgrounds.'),
    '#default_value' => $settings['independent_containers'],
    '#prefix' => '<div class="inline-label">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => 'settings_trigger'),
  );

  //For independent containers set the number of columns
  $containers = array(
      'header_classes' => t('Header Container'),
      'content_top_classes' => t('Content Top Container'),
      'content_classes' => t('Content Container'),
      'content_bottom_classes' => t('Content Bottom Container'),
      'footer_classes' => t('Footer Container'),
  );
  $form['containers_fieldset']['independent_containers_values']['containers_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Regions'),
    '#collapsible' => false,
    '#collapsed' => false,
    '#description' => t('If container by region checkbox is selected, set the number of columns for each region.'),
    '#attributes' => array('class' => 'containers-regions hidden-settings'),

  );
  foreach($containers as $key => $name){
    $form['containers_fieldset']['independent_containers_values']['containers_group'][$key] = array(
      '#type' => 'select',
      '#title' => t('@key_name ',array('@key_name' => $name)),
      '#options' => array(
  'none' => t('none'),
  'container_12' => t('12'),
  'container_16' => t('16'),
  'container_24' => t('24'),
      ),
      '#default_value' => $settings[$key] ? $settings[$key] : $settings['container_columns'],
    );
  }

  $form['frontpage_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page Settings'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['frontpage_settings']['frontpage_layout_checkbox'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Front Page Layout.'),
    '#default_value' => $settings['frontpage_layout_checkbox'],
    '#description' => t('Values for the grid can be set independently for the front page allowing painless layout configuration for a custom home page.'),
  );
  $form['frontpage_settings']['frontpage_hide_sidebars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide sidebars in the front page'),
    '#default_value' => $settings['frontpage_hide_sidebars'],
  );

  // grid settings form

  $grid_options = array(
    'header',
    'content_top',
    'content',
    'main',
    'sidebar_first',
    'sidebar_second',
    'content_bottom',
    'footer',
  );

  $layouts = array(
    'both_sidebars',
    'sidebar_first',
    'sidebar_second',
    'no_sidebars',
    'frontpage'
  );

  foreach ($layouts as $key) {

    $new_grid_options = $grid_options;
    if ($key == 'no_sidebars' || $key == 'sidebar_first') {
      unset($new_grid_options[5]);
    }
    if ($key == 'no_sidebars' || $key == 'sidebar_second') {
      unset($new_grid_options[4]);
    }

    $name = str_replace('_', ' ',$key);
    $form[$key .'_layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('@name Active', array('@name' => $name)),
      '#collapsible' => true,
      '#collapsed' => true,
      '#tree' => true,
    );

    // Grids Fieldset
    $form[$key .'_layout']['grids_settings'] = array(
      '#type' => 'fieldset',
      '#theme' => 'grids_settings_table',
    );
    $max_columns = ($settings['container_columns'] == 'container_12') ? 12 : 16;
    $max_columns = ($settings['container_columns'] == 'container_24') ? 24 : 16;
    for ($i = 0; $i <= $max_columns; $i++) {
      $options_16[$i] = $i == 0 ? t('none') : $i;
    }

    foreach ($new_grid_options as $key_region) {
      $name = str_replace('_', ' ',$key_region);
      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['grid'] = array(
        '#type' => 'select',
        '#title' => t('@name', array('@name' => $name)),
        '#attributes' => array('class' => 'input-inline'),
        '#options' => $options_16,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['grid'],
      );

      // prefix, suffix settings
      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['prefix'] = array(
        '#type' => 'select',
        '#attributes' => array('class' => 'input-inline'),
        '#options' => $options_16,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['prefix'],
      );

      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['suffix'] = array(
        '#type' => 'select',
        '#attributes' => array('class' => 'input-inline'),
        '#options' => $options_16,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['suffix'],
      );

      // push , pulls selects
      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['push'] = array(
        '#type' => 'select',
        '#attributes' => array('class' => 'input-inline'),
        '#options' => $options_16,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['push'],
      );

      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['pull'] = array(
        '#type' => 'select',
        '#attributes' => array('class' => 'input-inline'),
        '#options' => $options_16,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['pull'],
      );

      // alpha, omega checkboxes
      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['alpha'] = array(
        '#type' => 'checkbox',
        '#attributes' => array('class' => 'input-inline'),
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['alpha'],
      );

      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['omega'] = array(
        '#type' => 'checkbox',
        '#attributes' => array('class' => 'input-inline'),
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['omega'],
      );

      // extra classes textfield
      $form[$key .'_layout']['grids_settings'][$key_region .'_grid']['extras'] = array(
        '#type' => 'textfield',
        '#attributes' => array('class' => 'input-inline'),
        '#size' => 10,
        '#default_value' => $settings[$key .'_layout']['grids_settings'][$key_region .'_grid']['extras'],
      );
    }
    if(is_array($settings[$key .'_layout']['blocks_settings'])){
      $form[$key .'_layout']['blocks_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Layout Blocks'),
        '#description' => t('Blocks with grid values inside this Layout.'),
      );
      $blocks_settings = $settings[$key .'_layout']['blocks_settings'];
      $actions = array(
        'grid',
        'prefix',
        'suffix',
        'push',
        'pull',
        'extras',
      );
      foreach($blocks_settings as $block_id => $grid_data){
        $form[$key .'_layout']['blocks_settings'][$block_id] = array(
            '#type' => 'fieldset',
            '#title' => block_title($block_id) != ''  ? block_title($block_id) : $block_id,
            '#collapsible' => true,
            '#collapsed' => true,
        );
        foreach($grid_data as $action => $value){
          if(isset($value)){
              $form[$key .'_layout']['blocks_settings'][$block_id][$action] = array(
                  '#type' => 'textfield',
                  '#title'=> $action,
                  '#default_value' => $value,
                  '#size' => $action != 'extras' ? 6 : 40,
              );
          }
        }

      }
    }
  }

  return $form;
}

// Returns block titles for opengrid_ui module
// TODO: move this to the opengrid_ui module and use a hook instead
function block_title($delta){
  $pos = strpos($delta, 'block-block-');
  if($pos == 0){
      $delta = str_replace('block-block-','', $delta);
      $module = 'block';
  }
  $block = db_fetch_array(db_query("SELECT title FROM {blocks} WHERE module = '%s' AND delta = '%s'", $module, $delta));
  return $block['title'];
}
