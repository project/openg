<?php

require_once('theme-settings.php');

/**
 * Initialize theme settings.
 */
global $theme_key;
opengrid_initialize_theme_settings($theme_key);

$theme_settings = theme_get_settings($theme_key);
$grid_type = (theme_get_setting('css_switch') == 0) ? 'fixed' : 'fluid';
$grid_columns = $theme_settings['container_columns'];
drupal_add_css(drupal_get_path('theme', 'opengrid') .'/css/'. $grid_type .'_'. $grid_columns .'.css');


// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('opengrid_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}

if (theme_get_setting('scroll_to_top') == 1 && !(arg(2) == 'views' && arg(3) =='edit')) {
  drupal_add_js(drupal_get_path('theme', 'opengrid') .'/js/scrolToTop.js');
}

if (theme_get_setting('hide_sidebars') == 1) {
  drupal_add_js(drupal_get_path('theme', 'opengrid') .'/js/layout.js');
}

//Superfish settings
if (theme_get_setting('dropdown_options') == 0 || theme_get_setting('dropdown_options') == 1) {

  static $hoverIntent_script = FALSE;
  if (!$hoverIntent_script) {
    drupal_add_js(drupal_get_path('theme','opengrid').'/js/jquery.hoverIntent.minified.js');
    $hoverIntent_script = TRUE;
  }

  drupal_add_js(drupal_get_path('theme','opengrid').'/js/supersubs.js');
  drupal_add_js(drupal_get_path('theme','opengrid').'/js/superfish.js');
  drupal_add_css(drupal_get_path('theme','opengrid').'/css/superfish.css');

  drupal_add_js(array('superfish_settings' => array(
    'superfish' => true,
    'sf_hi' => theme_get_setting('sf_hi') ? false : true,
    'sf_dropshadows' => theme_get_setting('sf_dropshadows') ? true : false,
    'sf_delay' => theme_get_setting('sf_delay'),
    'sf_speed' => theme_get_setting('sf_speed'),
    'sf_subs_min_width' => theme_get_setting('sf_subs_min_width'),
    'sf_subs_max_width' => theme_get_setting('sf_subs_max_width'),
    'sf_subs_centralized' => theme_get_setting('sf_subs_centralized'),
    )), 'setting');
}

if (theme_get_setting('dropdown_options') == 2) {
  drupal_add_js(array('superfish_settings' => array('superfish' => false)), 'setting');
}


// Adds external font file to the head
$font_file_url = theme_get_setting('font_file_url');
  if($font_file_url){
      drupal_add_link(array('type' => 'text/css', 'rel' => 'stylesheet', 'href' => $font_file_url ));
  }

/********************************************************
* Return a themed breadcrumb trail.
*
* @param $breadcrumb
*   An array containing the breadcrumb links.
* @return
*   A string containing the breadcrumb output.
**********************************************************/
function opengrid_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('show_breadcrumbs');
  if ($show_breadcrumb == 1 && !drupal_is_front_page()) {
    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('breadcrumb_separator');
      $trailing_separator = '';
      if (theme_get_setting('breadcrumb_title')) {
    $title = theme_get_setting('breadcrumb_title') . $breadcrumb_separator;
          $trailing_separator = $breadcrumb_separator;
      }
      return '<div class="breadcrumb">' . $title . implode($breadcrumb_separator, $breadcrumb) . "</div>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}


/**
 * Implements HOOK_theme().
 */
function opengrid_theme(&$existing, $type, $theme, $path) {
  if (!db_is_active()) {
    return array();
  }
  $items = array();
  $items['containers_settings_table'] = array(
    'arguments' => array('form' => NULL),
  );
  $items['grids_settings_table'] = array(
    'arguments' => array('form' => NULL),
  );
  $items['system_settings_form'] = array(
    'arguments' => array('form' => NULL),
  );
  $items['comment_form'] = array(
    'arguments' => array('form' => NULL),
  );

  return $items;
}

/**
 * Implements HOOK_preprocess().
 */
function opengrid_preprocess(&$vars, $hook) {
  if ($hook == 'block') {
    $regions = array();
    //Code to detect regions activated by block instances module
    //http://drupal.org/project/block_instances
    if (module_exists('block_instances')) {
      $blocks_instances = block_instances_crud_load_from_region('sidebar_first');
      foreach($blocks_instances as $instance){
        if (block_instance_render_block($instance)) {
          $regions[$instance->region] = $instance->region;
        }
      }
      $blocks_instances = block_instances_crud_load_from_region('sidebar_second');
      foreach($blocks_instances as $instance){
        if (block_instance_render_block($instance)) {
          $regions[$instance->region] = $instance->region;
        }
      }
    }
    //Code to detect regions activated by context module
    //http://drupal.org/project/context
      if (module_exists('context')) {
        foreach (context_active_contexts() as $context) {
          if (is_array($context->reactions['block']['blocks'])) {
            foreach ($context->reactions['block']['blocks'] as $block) {
                $regions[$block['region']] = $block['region'];
            }
          }
        }
      }
    $vars['my_layout'] = 'no_sidebars';

    if (block_list('sidebar_first') || in_array('sidebar_first',$regions)) {
      $vars['my_layout'] = 'sidebar_first';
    }

    if (block_list('sidebar_second') || in_array('sidebar_second', $regions)) {
      $vars['my_layout'] = ($vars['my_layout'] == 'sidebar_first') ? 'both_sidebars' : 'sidebar_second';
    }

    if (drupal_is_front_page()) {
      $theme = variable_get('theme_default','none');
      $settings = theme_get_settings($theme);
      $vars['my_layout'] = ($settings['frontpage_layout_checkbox'] == 1) ? 'frontpage' : $vars['my_layout'];
    }
  }
}

/**
 * Hook to preprocess Page
 */
function opengrid_preprocess_page(&$vars) {

  //Labelify call
  if (opengrid_labelify()) {
    $vars['scripts'] = drupal_get_js();
  }

  $theme = variable_get('theme_default','none');

  //Adds an anchor to node form in node preview page
  if (arg(0) == 'node' && arg(3) == 'preview') {
    $node_form_anchor = l(t('[Continue Editing]'), $_GET['q'], array('attributes' => array('title' => t('Anchor to Node Form'), 'class' => 'edit-link'),'fragment' => 'node-form'));
    $vars['title'] = t('Preview of: ') . $vars['title'] . $node_form_anchor;

    $vars['body_classes'] = explode(' ', $vars['body_classes']);
    $vars['body_classes'][] = 'page-node-preview';
    $vars['body_classes'] = implode(' ', $vars['body_classes']);
  }

  //Creates copyright text in the footer
  $start = theme_get_setting('start');
  if ($start) {

    $copyright_year = ($start != date('Y')) ? check_plain($start) .' - '. date('Y') : check_plain($start);

    global $base_url;
    $front_page = (str_replace(array('http://','https://'), '', $base_url));
    $copyright_name = theme_get_setting('copyright_name') ? theme_get_setting('copyright_name') : $vars['site_name'] .' - '. $front_page;
    $vars['copyright'] = '&#169; '. $copyright_year .' - '. $copyright_name;
  }

  // Put the text in a variable for translations support
  $vars['skip_to_content'] = t('Skip to Main Content');

  // Strip duplicate head charset metatag(http://drupal.org/node/451304#comment-3885598)
  $matches = array();
  preg_match_all('/(<meta http-equiv=\"Content-Type\"[^>]*>)/', $vars['head'], $matches);
  if ( count($matches) >= 2) {
    $vars['head'] = preg_replace('/<meta http-equiv=\"Content-Type\"[^>]*>/', '', $vars['head'], 1); // strip 1 only
    $vars['head'] = preg_replace('/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/', '', $vars['head']);
  }
  // As of Drupal 7, for the sake of better semantics and RTL goodness the
  // sidebars have been renamed from $sidebar_left/right to $sidebar_first/second
  // We need to re-do the $layout and body classes because
  // template_preprocess_page() assumes sidebars are named 'left' and 'right'.
  $vars['layout'] = 'none';
  if (!empty($vars['sidebar_first'])) {
    $vars['layout'] = 'first';
  }
  if (!empty($vars['sidebar_second'])) {
    $vars['layout'] = ($vars['layout'] == 'first') ? 'both' : 'second';
  }

  // If the layout is 'none', then template_preprocess_page() will already have
  // set a 'no-sidebars' class since it won't find a 'left' or 'right' sidebar.
  $vars['body_classes'] = explode(' ', $vars['body_classes']);
  if ($vars['layout'] != 'none') {
    // Remove the incorrect 'no-sidebars' class.
    if ($index = array_search('no-sidebars', $vars['body_classes'])) {
      unset($vars['body_classes'][$index]);
    }
    // Set the proper layout body classes.
    if ($vars['layout'] == 'both') {
      $vars['body_classes'][] = 'two-sidebars';
    }
    else {
      $vars['body_classes'][] = 'one-sidebar';
      $vars['body_classes'][] = 'sidebar-' . $vars['layout'];
    }
  }
  //set a class when secondary links are active
  if ($vars['secondary_links']) {
    $vars['body_classes'][] = 'sublinks-nav';
  }

  //Add Class for body in node add pages
  if ((arg(1) == 'node' && arg(2) == 'add')) {
    $vars['body_classes'][] = 'add-'. arg(3);
  }
  if ((arg(0) == 'node' && arg(1) == 'add')) {
    $vars['body_classes'][] = 'add-'. arg(2);
  }

  $vars['body_classes'] = implode(' ', $vars['body_classes']);

  // adds classes to primary links configured in theme settings
  if (theme_get_setting('navigation_extras') != '') {
    $vars['main_navigation_classes'] .= ' '. theme_get_setting('navigation_extras');
  }
  if (theme_get_setting('navigation_classes') != 'none') {
    $vars['main_navigation_classes'] .= ' '. theme_get_setting('navigation_classes');
  }

  $vars['primary_links'] = $vars['primary_links'] ? theme('links',$vars['primary_links']) : '';
  $vars['secondary_links'] = $vars['secondary_links'] ? theme('links',$vars['secondary_links']) : '';

  //rewrite primary links for dropdown support
  if (theme_get_setting('dropdown_options') == 1 || theme_get_setting('dropdown_options') == 0) {
    $vars['primary_links'] = $vars['primary_links'] ? menu_tree(variable_get('menu_primary_links_source', 'primary-links')) : '';
    $vars['secondary_links'] = (theme_get_setting('dropdown_options') == 0) ? '' : $vars['secondary_links'];
    $vars['body_classes'] = (theme_get_setting('dropdown_options') == 0) ? str_replace('sublinks-nav','', $vars['body_classes']) : $vars['body_classes'];
    $vars['body_classes'] = (theme_get_setting('dropdown_options') == 1) ? $vars['body_classes'] .' hide-dropdown' : $vars['body_classes'];
  }

  //loads grid values from the theme_settings variable
  if (is_file(drupal_get_path('theme', 'opengrid') . '/opengrid_grid_assembler.inc')) {
    include('opengrid_grid_assembler.inc');
    $vars = opengrid_grid_settings_vars($vars);
  }

  //Pages set without sidebars
  $saved_paths = trim(theme_get_setting('hide_sb_paths'));
  $path = drupal_get_path_alias($_GET['q']);
  // Compare with the internal and path alias (if any).
  $page_match = drupal_match_path($path, $saved_paths);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $saved_paths);
  }
  if ($page_match || (drupal_is_front_page() && theme_get_setting('frontpage_hide_sidebars'))) {
    $layout_type = (drupal_is_front_page() && theme_get_setting('frontpage_hide_sidebars')) ? 'frontpage_layout' : 'no_sidebars_layout';
    $settings = theme_get_settings($theme);
    $layout = layout_grid_assembler($layout_type, $settings);
    if ($vars['sidebar_first'] || $vars['sidebar_second']) {
      $body_classes = $vars['body_classes'] .' no-sidebars';
      $body_classes = ($vars['sidebar_first'] || $vars['sidebar_second']) ? str_replace('one-sidebar','',$body_classes) : $body_classes;
      $body_classes  = $vars['sidebar_first'] ? str_replace('sidebar-first','',$body_classes) : $body_classes;
      $body_classes  = $vars['sidebar_second'] ? str_replace('sidebar-second','',$body_classes) : $body_classes;
      $vars['body_classes'] = ($vars['sidebar_first'] && $vars['sidebar_second']) ? str_replace('both-sidebars','no-sidebars', $body_classes) : $body_classes;
    }
    $vars['sidebar_first'] = NULL;
    $vars['sidebar_second'] = NULL;
    $container_value = theme_get_setting('container_columns');
    $container_value = str_replace('container','grid', $container_value);
    $vars['main_grid'] =  $layout['main_grid'];
  }

  // Add liquid-layout class to the body
  if (theme_get_setting('css_switch') == 1) {
    $vars['body_classes'] .= ' liquid-layout';
  }
  if (theme_get_setting('scroll_to_top') == 1 && !(arg(2) == 'views' && arg(3) =='edit')) {
    $vars['scroll_to_top'] = t('Scroll to Top');
  }
  //body classes adjustments
  $vars['body_classes'] = str_replace('not-front','',$vars['body_classes']);
  if (arg(0) == 'node' && arg(1) == 'add') {
    $vars['body_classes'] .= ' node-add-section';
  }
  if (arg(0) == 'node' && arg(2) == 'edit') {
    $vars['body_classes'] .= ' node-edit-section';
  }
  if (theme_get_setting('og_guides_bg')) {
    $vars['body_classes'] .= ' og-guides-bg';
  }
  if (theme_get_setting('og_guides_borders')) {
    $vars['body_classes'] .= ' og-guides-borders';
  }

  if (module_exists('page_title')) {
    $vars['head_title'] = page_title_page_get_title();
  }

  //modifing local task in a similar way they are in Garland theme
  if (menu_primary_local_tasks()) {
    $output = "<ul class=\"tabs primary\">\n". $vars['tabs'] ."</ul>\n";
    $vars['tabs'] = $output;
    $vars['with_tabs'] = $vars['tabs'] ? 'with-tabs' : 'drupal-tabs';
  }
  if (menu_secondary_local_tasks()) {
    $vars['tabs2'] = menu_secondary_local_tasks();
  }

  global $base_url;
  $front_page = $base_url;
  //logo overwrite
  $logo = $vars['logo'];
  if ($logo) {
    $logo_img = '<img class="logo-img" src="'. $logo .'" alt="'. $vars['site_name'].'" title="'. $vars['home'].'" />';
    $vars['logo'] = drupal_is_front_page() ? $logo_img : l($logo_img, $front_page, array('html' => true, 'attributes' => array('class' => 'logo-img','title' => $vars['home'])));
  }

  //sitename bg image
  $vars['sitename'] = $vars['site_name'];
  $sitename_path = theme_get_setting('sitename_path');
  if ($sitename_path) {
    $vars['sitename_path'] = '/'. $sitename_path;
    $vars['sitename_alt'] = theme_get_setting('sitename_alt') ? theme_get_setting('sitename_alt') : $vars['sitename'];
    $vars['sitename_title'] = theme_get_setting('sitename_title');
    $site_name_img = '<img title="'. $vars['sitename_title'] .'" alt="'. $vars['sitename_alt'] .'" src="/'. $sitename_path .'?v=1" />';
  }
  $site_name = $site_name_img ? $site_name_img : $vars['sitename'];
  if(drupal_is_front_page()){
    $vars['site_name'] = $site_name;
  }
  else {
    $vars['site_name'] = l($site_name, $front_page, array('html' => true, 'attributes' => array('title' => $vars['site_name'] . t(' home page'))));
  }

  //Prints base text from themes settings to footer
  $base_text = theme_get_setting('base_text');
  if($base_text){
    $vars['base_text'] = $base_text;
  }

}

/**
 * Hook to preprocess Node
 */
function opengrid_preprocess_node(&$vars) {
  //Adds a new regions below the node content and above the comments
  // and above node title
  $vars['content_prefix'] = theme('blocks', 'content_prefix');
  $vars['content_suffix'] = theme('blocks', 'content_suffix');

  $vars['node_classes'] = $vars['page'] ? 'node' : 'teaser';

  //Status Text in Preview pages for nodes
  if (arg(0) == 'node' && (arg(3) == 'preview' || $vars['node']->status == 0)) {
    $vars['status_flag'] = t('Status: ') . ($vars['node']->status == 1 ? t('Published') : t('Unpublished'));
  }
}

/**
 * Hook to preprocess Block
 */
function opengrid_preprocess_block(&$vars) {

  $theme = variable_get('theme_default','none');
  $block = $vars['block'];

  $block_id = 'block-'. $block->module .'-'. $block->delta;

  $vars['block_id'] = $block_id;
  $vars['block_classes'] .= ' block block-'. $block->module;

  //Loads the variables thats controls the grid settings for the blocks
  if (!is_file(drupal_get_path('theme', $theme) . '/opengrid_blocks.inc')) {
    $settings = variable_get('theme_'. $theme .'_settings',array());

    $block_classes = $settings[$vars['my_layout'].'_layout']['blocks_settings'][$block_id];

    if (is_array($block_classes)) {
      foreach ($block_classes as $op => $value) {
          $classes_array[$op] = ($op != 'extras') ? $op .'_'. $value : $value;
      }
      $classes = implode(' ', $classes_array);
      $vars['block_classes'] .= ' '. $classes;
    }
  }
  //load grid values for the grid from code, if the file exists
  if (is_file(drupal_get_path('theme', $theme) . '/opengrid_blocks.inc')) {
      $layout = $vars['my_layout'] .'_layout';
      include(drupal_get_path('theme', $theme) . '/opengrid_blocks.inc');
  }
}

/**
 * Hook to preprocess comment wrapper
 */
function opengrid_preprocess_comment_wrapper($vars){
  $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
  $comments_title = check_plain(theme_get_setting('comments_wrapper_title'));
  $plural = $comments_title ."'s";

  if (theme_get_setting('comments_counter') && ($vars['node']->comment_count != 0)) {
    $plural = format_plural($vars['node']->comment_count, '1 '. $comments_title, '@count '. $comments_title ."'s");
  }
  $comments_title = ($vars['node']->comment_count == 0) ? check_plain(theme_get_setting('comments_empty_text')) : $plural;
  $vars['comment_title'] = t('@title',array('@title' => $comments_title));

  $form_bellow = variable_get('comment_form_location_'. $vars['node']->type, 0);
  if ($form_bellow && ($vars['node']->comment_count != 0)) {
    $comments_string = ($vars['node']->comment_count == 0) ? check_plain(theme_get_setting('comments_empty_text')) : check_plain(theme_get_setting('comments_string'));
    $vars['comments_message'] = l(t('@title',array('@title' => $comments_string)),$path ,array('fragment' => 'comment-form'));
  }
}
/********************************
 * Hook to preprocess comment
*********************************/
function opengrid_preprocess_comment(&$vars){
  if (theme_get_setting('comments_subject') == 0) {
    $vars['title'] = '';
  }
}
/**
 * Hook to preprocess Box
 */
function opengrid_box($title, $content, $region = 'main') {
  $title = ($title == 'Post new comment') ? '' : '<h2 class="title">' . $title . '</h2>';
  $output = $title .'<div>' . $content . '</div>';
  return $output;
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function opengrid_menu_local_tasks() {
  return menu_primary_local_tasks();
}

/**
 * Returns a table to configure container settings.
 */
function opengrid_containers_settings_table(&$form) {
  $rows = array();
  $header = array('', t('Columns'),t('Extras'));

  foreach(element_children($form) as $key) {
    $name = $form[$key]['grid']['#title'];
    $form[$key]['grid']['#title'] = '';
    $form[$key]['extras']['#title'] = '';
    $row = array(
      $name,
      drupal_render($form[$key]['grid']),
      drupal_render($form[$key]['extras']),
    );
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  return $output;
}

/**
 *  Returns a table to configure grid settings.
 */
function opengrid_grids_settings_table($form) {
  $rows = array();
  $header = array (
    t('Region'),
    t('Width'),
    t('Prefix'),
    t('Suffix'),
    t('Push'),
    t('Pull'),
    t('Alpha'),
    t('Omega'),
    t('Extras'),
  );

  foreach (element_children($form) as $key) {
    $name = $form[$key]['grid']['#title'];

    $form[$key]['grid']['#title'] = '';
    $container = t('No container');
    if ($key != 'main_grid' && $key != 'first_sidebar_grid' && $key != 'second_sidebar_grid') {
      $container = drupal_render($form[$key]['container']);
    }
    $row = array(
      $name,
      drupal_render($form[$key]['grid']),
      drupal_render($form[$key]['prefix']),
      drupal_render($form[$key]['suffix']),
      drupal_render($form[$key]['push']),
      drupal_render($form[$key]['pull']),
      drupal_render($form[$key]['alpha']),
      drupal_render($form[$key]['omega']),
      drupal_render($form[$key]['extras']),
    );
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * File element override
 * Sets form file input max width
 */
function opengrid_file($element) {
  $element['#size'] = ($element['#size'] > 40) ? 40 : $element['#size'];
  return theme_file($element);
}


function opengrid_node_form($form) {
  //fixed position forsubmit buttons
  if (theme_get_setting('fixed_buttons') == 1) {
    $form['buttons']['#prefix'] = '<div class="buttons-wrapper">';
    $form['buttons']['#suffix'] = '</div>';

  }
  $form['buttons']['#weight'] = 50;
  return drupal_render($form);
}

/********************************
 * Hook theme username
 * removes "not verify" text.
*********************************/
function opengrid_username($object) {

  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) . '...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/' . $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }

    if (!theme_get_setting('comments_user_name')) {
      $output .= ' (' . t('not verified') . ')';
    }

  }
  else {
    $output = check_plain(variable_get('anonymous', t('Anonymous')));
  }

  return $output;
}
/*
* hook to theme message
* Return a themed set of status and/or error messages. The messages are grouped by type.
* @display (optional) Set to 'status' or 'error' to display only messages of that type.
* return: A string containing the messages.
*/

function opengrid_status_messages($display = NULL) {
  if (module_exists('jquery_ui') && theme_get_setting('popup_messages')) {
    jquery_ui_add('ui.draggable');
    drupal_add_js(drupal_get_path('theme', 'opengrid') .'/js/opengridMessages.js');
    $draggable_class = 'ui-draggable';
  }

  $output = '';

  foreach (drupal_get_messages($display) as $type => $messages) {
    $type = ($type == 'status' && theme_get_setting('auto_hide_messages') == 1) ? 'Status' : $type;
      $output .= "<div class=\"messages $draggable_class $type \">\n";
      $output .= '<div class="inner-message">';
      $output .= "<h2 class=\"messages-label $type\">" . t(drupal_ucfirst($type)) . "</h2>\n";
      if (count($messages) > 1) {
        $output .= " <ul>\n";
        foreach ($messages as $message) {
          $output .= '  <li>'. $message ."</li>\n";
        }
        $output .= " </ul>\n";
      }
      else {
        $output .= $messages[0];
      }
      if (module_exists('jquery_ui')) {
        $output .= '<span class="close">'. t('Close') .'</span></div>';
      }
    $output .= "</div>\n";
  }

  if (theme_get_setting('popup_messages')) {
    $auto_hide =  (theme_get_setting('auto_hide_messages') == 1) ? ' message-autohide' : '';
    $output = '<div class="messages-wrapper'. $auto_hide .'">'. $output .'</div>';
  }

  return $output;
}
/*
* Collapsing Default fields of theme settings form
*/
function opengrid_system_settings_form($form) {
  // Collapse fieldsets
  $form_elements = element_children($form);
  //Identify fieldsets and collapse them
  foreach ($form_elements as $element) {
    if ($form[$element]['#type'] == 'fieldset') {
      $form[$element]['#collapsible'] = TRUE;
      $form[$element]['#collapsed']   = TRUE;
    }
  }
  //fixed position forsubmit buttons
  if (theme_get_setting('fixed_buttons') == 1) {
    $form['buttons']['#prefix'] = '<div class="buttons-wrapper">';
    $form['buttons']['#suffix'] = '</div>';
  }
  return drupal_render($form);
}

/*
* Editing Label for Comment Form
*/
function opengrid_comment_form($form){
  $form['_author']['#title'] = t('Name');
  $form['name']['#title'] = t('Name');
  return drupal_render($form);
}
/*
* Drupal 7 Backported code
*/
function opengrid_form_element($element, $value) {
  $t = get_t();

  $output = '<div class="form-item"';
  if (!empty($element['#id'])) {
    $output .= ' id="'. $element['#id'] .'-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="'. $t('This field is required.') .'">*</span>' : '';

  $error = '';
  if ($element['#type'] == 'password' && $element['#name'] == 'pass[pass1]') {
    // Only display an error on the second password field.
  } else if (!empty($element['#required']) && empty($element['#value'])) {
    $error = form_get_error($element) ? '<span class="error">' . $t('Field is required.') . '</span>' : '';
  } else {
    $error = form_get_error($element) ? '<span class="error">' . filter_xss_admin(form_get_error($element)) . '</span>' : '';
  }

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="' . $element['#id'] . '">' . $t('!title !required !error', array('!title' => filter_xss_admin($title), '!required' => $required, '!error' => $error)) . "</label>\n";
    }
    else {
      $output .= ' <label>' . $t('!title !required !error', array('!title' => filter_xss_admin($title), '!required' => $required, '!error' => $error)) . "</label>\n";
    }
  }

  $output .= " $value\n";

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">'. $element['#description'] ."</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

function opengrid_item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {

  if ($attributes['class'] == 'pager')
  {
  $output = '<h2 class="element-invisible">Pages</h2><div class="item-list pager">';

  }else
  {
  $output = '<div class="item-list">';

  }

  if (isset($title)) {
    $output .= '<h3>'. $title .'</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type". drupal_attributes($attributes) .'>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else $attributes[$key] = $value;
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      if ($i == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] .' first');
      }
      if ($i == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] .' last');
      }
      $output .= '<li'. drupal_attributes($attributes) .'>'. $data ."</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

function opengrid_preprocess_search_result(&$variables) {
  $result = $variables['result'];
  $variables['url'] = check_url($result['link']);
  $variables['title'] = check_plain($result['title']);
  $info = array();
  if (!empty($result['date'])) {
    $info['date'] = format_date($result['date'], 'small');
  }
  if (isset($result['extra']) && is_array($result['extra'])) {
    $info = array_merge($info, $result['extra']);
  }
  // Check for existence. User search does not include snippets.
  $variables['snippet'] = isset($result['snippet']) ? $result['snippet'] : '';
  // Provide separated and grouped meta information..
  $variables['info_split'] = $info;
  $variables['info'] = implode(' - ', $info);
  // Provide alternate search result template.
  $variables['template_files'][] = 'search-result-'. $variables['type'];
}

/***********************************************
*** Labelify
***********************************************/
/*
* Loads the Id's of forms to be labelify
* and the js files to do the job.
*/
function opengrid_labelify(){

  if (arg(1) != 'edit') {
    $theme = variable_get('theme_default','none');
    static $labelify_script = FALSE;
    $settings = theme_get_settings($theme);

    $labelify_theme_settings = array(
      'labelify_forms' => is_array($settings['labelify_ids']) ? $settings['labelify_ids'] : '',
    );

    if (is_array($labelify_theme_settings['labelify_forms']) && !empty($labelify_theme_settings['labelify_forms'])) {
      drupal_add_js(array('labelify_theme_settings' => $labelify_theme_settings), 'setting');
      if(!$labelify_script){
        drupal_add_js(drupal_get_path('theme','opengrid').'/js/jquery.labelify.js');
        $labelify_script = true;
      }

      return true;
    }
    if (empty($labelify_theme_settings['labelify_forms'])) {
      drupal_add_js(array('labelify_theme_settings' => $labelify_theme_settings), 'setting');
      static $labelify_script = FALSE;
      return false;
    }
  }
  else {
    drupal_add_js(array('labelify_theme_settings' => array('labelify_forms' => '')), 'setting');
  }
}

