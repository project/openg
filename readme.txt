OPENG (OPENGRID) - DRUPAL THEMING SIMPLICITY

In this document

*** INTRODUCTION
*** THE BASICS
*** GRID TERMINOLOGY
*** CONFIGURING THE GRID
*** PRE-CONFIGURED GRIDS
*** OPENGRID UI
*** EXPORTING SETTINGS TO CODE
*** THEME WIDGETS & UTILITY GOODNESS
*** ORGANIZATION OF CSS
*** SEMANTIC CODE
*** FUTURE COMPATIBLE
*** HOW TO USE THE UTILITY TRAY REGION


*** INTRODUCTION ***

Thanks for trying out OpenG. We're really happy to share this starter theme
with you - and with your help, hope to steadily improve on the work done so far.

We've been theming on Drupal since D4.6, way back in 2004 and have
never really been 100% satisfied with any of the starter themes. We
discovered that it had nothing to do with the themes, instead it had to do with
our style.

Every designer will eventually form their own style. Some of the
beautiful starter themes out there like Zen, AdaptiveTheme, Basic,
Foundation, Genesis, Fusion et al. cater for many of these styles.

Based on the work of the awesome community of designers and themers
at D.O, we created and are creating a theme that suits our style, and hopefully
yours too.

We'd love to hear what you think about the theme. Please post your
thoughts, ideas, suggestions and complaints on the project issue
queue and one of us will surely reply.

To your Drupal theming happiness!

The Mojah Media Team
http://mojahmedia.com


*** THE BASICS ****

OpenG is a grid based theme, which is to say it uses a system of
vertical columns as guides for the designer or site architect. The
grid system is not a solution in itself. It is instead a powerful
tool to help designers visually layout design elements according to the
project information architecture.

Needless to say, but I'll say it anyway, one would already have a
clear plan of their project before even getting anywhere near to
theming Drupal. Before one gets here, one would already have at
hand the objectives of the home page, the various site sections,
the principal navigation elements et. al.

With this information, the grid system will help one spatially
arrange core and custom Drupal elements within the space available.

OpenG being both a fixed (set to 960px) and liquid (spreads across
the entire available browser width) allows the designer to allocate
grid settings to regions. OpenG provides a choice of 12, 16 and 24
grid columns, with the 24 column layout providing the highest level of
refinement in layout.

OpenG provides 9 common regions (both nested and wrapper regions) and
1 utility region that expands and collapses. Once a grid value has
been allocated to a region, the region will align it's left side to
the defined column and stretch it's width to that of the number of
columns assigned to it or it's wrapping container. These grid values
control the positioning of regions via a complex system of CSS rules
which the designer never has to see.

As a bonus, OpenG also allows grid values to be applied to Drupal blocks.
This allows for the development of interesting and highly complex layouts.


*** GRID TERMINOLOGY ***

grid - a value that defines an elements the horizontal space (width) within the grid

prefix/suffix - left and right padding applied to the element

push/pull - left and right values applied to elements ie .class { left:30px; }

omega/alpha - the grid system by default has a left and right margin applied
to every element that gas a grid_xx setting. Omega removes the left margin
and alpha removes the right margin. The classes .alpha and .omega have to be
manually inserted into the corresponding region via theme settings or via
the Opengrid UI


*** CONFIGURING THE GRID ***

Grid settings can be configured via the theme settings page. Go to:
/admin/build/themes/settings/opengrid and look for the OpenG Grid Settings.
section. Select the number of grid columns then save. Return to this page
and update the grid settings for the types of layouts available.


*** PRE-CONFIGURED GRIDS ***

Before to enable the theme you can select the Grid you want to start to
work with. Default values for the theme are set for a 16 columns width,
but you can start working with a 12 or 24 columns Grid. Default values
for those Grids can be loaded, selecting one of the alternatives .info
files located in the root of the theme folder.

Example.
To start working with the 24 columns Grid:
Rename opengrid.info to opengrid.16columns.info
Rename opengrid.24columns.info to opengrid.info

Note: You can change the number of columns for the Grid from the theme
settings page, but if you do so, remember to re-adjust the values of the
grid for the regions in the different layouts, otherwise the default values
won't work for the new  grid configuration.


*** OPENGRID UI ***

http://drupal.org/project/opengrid_ui - The following section is related
this module which works closely with the OpenG theme by providing a jquery
interface to configure the grid visually

*** EXPORTING SETTINGS TO CODE ***

When you edit the values for the grid from the theme-settings page or
the Opengrid UI, those changes are saved in a variable in the data base.
Opengrid UI modules provides a way to move those new values to code for the
sake of portability. This effectively allows you to move carefully configured
layouts from one OpenG site to another.
On this screen "admin/build/opengrid-ui/export"
you'll find two text-areas:

Grid settings:
In this textarea is the code related to theme regions. Copy and paste
this code into the opengrid.info file, replacing the default values.

Block grid settings:
In this text-area are the values related to any block grid settings that
were configured via the Opengrid UI. Copy and paste this code in a file
named "opengrid_blocks.inc", and then save that file in the root folder
of the theme.

This way the configuration for the grid is independent of the data base
of your site, and you can export this Grid configuration to other sites.


*** THEME WIDGETS & UTILITY GOODNESS ***

We've incorporated several useful development and theming widgets, all
available via theme settings. Here is a brief list

- Sitename image: An image can be uploaded via theme-settings to replace the sitename
- Breadcrumbs: Switch on the breadcrumbs and configure the separator and prefix text
- Eternal Font: Load an external font file to use a custom font in theme design
- Copyright info: Have the copyright year automatically updated.
- Superfish drop-downs: Incorporates the elegant Superfish drop-down menu system
- Comment settings: toggle comment count and provides 3 string variables to override Drupals defaults
- OpenG UI Widgets: Scroll to top button, fixed submit buttons and auto-hide status messages
- Sidebar Hide Controls: Hide the sidebars on admin screens or provide a toggle
- Front Page settings: Create a custom front page layout and hide sidebars
- OpenG Developer Widgets: Rebuild registry and show background images and borders for visual guides


*** ORGANIZATION OF CSS ***

OpenG really only needs og.core.css, og.layout.css and the corresponding
grid 960_*.css to work. Every other style sheet is additional and can
be safely commented out from the opengrid.info file. The code is clean
and well commented.

Files

├── og.core.css -> required - all default html and Drupal related style
├── og.layout.css -> required - all spacial related rules
├── og.forms.css -> form specific rules
├── og.aesthetics.css -> links, colors, icons, images etc
├── og.extras.css -> some common modules, theming utilities and D7 back-ports
├── og.typography.css -> everything related to typography
├── reset.css -> a default reset style sheet for html elements
├── tinymce.css -> makes the tinymce wysiwyg look a little better
├── 960_16.css -> includes style for 12 column
├── 960_16_fluid.css -> includes style for 12 column
├── 960_24.css
├── 960_24_fluid.css
├── 960_fluid.css
└── readme.txt -> best practices for formatting of css


*** SEMANTIC CODE ***

The class names have been carefully created with semantics in mind. Reading
and editing the code is much easier this way


*** FUTURE COMPATIBLE ***

We're aiming for an easier upgrade path to Drupal 7 and have back-ported
several usability practices implemented in Drupal 7. There is also a sub-theme
with HTML 5 tags that "plugs in" to OpenG without any changes to code required


*** HOW TO USE THE UTILITY TRAY REGION ***

Important: A new region has been added to the theme "Tray Region". If
you are using a sub-theme of Opengrid in order to see the new region you
have to do two things:
1 - Add the new region into the .info file of your sub-theme
(regions[tray_region] = Tray Region)
2 - Add to the page.tpl.php file the code that prints the new region:

<?php if(!empty($tray_region)): ?>
<div class="tray-close">Tray</div>
<div id="tray-wrapper">
<?php print $tray_region; ?>
</div>
<?php endif; ?>

Paste before the closure variable.

