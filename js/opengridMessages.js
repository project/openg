Drupal.behaviors.opengridMessages= function(context) {

  $('.messages-wrapper:not(.processed)',context).addClass('processed messages-popup-active');

  $('.inner-message .close').hover(function(){
    $(this).addClass('hover');
  }, function() {
    $(this).removeClass("hover");
  });

  $('.inner-message .close').click(function(){
    $(this).parent().parent().fadeOut('slow');
  });

  $('.messages:not(.message-processed)',context).addClass('message-processed').each(function(){
    $(this).hide().fadeIn(1000);
    var t = setTimeout(function(){$('.message-autohide').fadeOut();}, 5000);
    $(window).click(function(){$('.messages-autohide').fadeOut();});

    //keeps the message shown when the mouse is on it.
    $(this).mouseenter(function(){
      clearTimeout(t);
      $('.message-autohide').stop();
    }).mouseleave(function(){
      t = setTimeout(function(){$('.message-autohide').fadeOut();}, 3000);
    });

    var options = {
      handle: '.inner-message h2'
    };

    $('.ui-draggable').draggable(options);

  });

}
