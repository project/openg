Drupal.behaviors.scrolToTop= function(context) {

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
&& location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target
      || $('[name=' + this.hash.slice(1) +']');
      if ($target.length) {
  var targetOffset = $target.offset().top;
  $('html,body').animate({scrollTop: targetOffset}, 1200);
  return false;
      }
    }
  });

  $(window).scroll(function() {
      var sd = $(window).scrollTop();

      if ( sd > 200 )
        $('.scroll-to-top').fadeIn(200);
      else
        $('.scroll-to-top').fadeOut(400);
  });


}
