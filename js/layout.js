Drupal.behaviors.layoutjs = function(context) {
  $('#main').prepend('<div class="region-switch">Hide Sidebars</div>');
  $('.region-switch').hide();
  $('#main:not(region-processed)',context).mouseover(function(){
    $(this).addClass('region-processed').css({'position':'relative'});
    $('.region-switch').show().css({'position':'absolute','top':'-10px'});
    $('.region-switch:not(processed)',context).addClass('processed').mouseover(function(){
      $(this).css({'color':'#000'});
    }).mouseout(function(){
      $(this).removeAttr('style');
    });
  }).mouseout(function(){
    $('.region-switch').hide();
  });
  $('.region-switch:not(.processed)',context).toggle(function(){
    $(this).addClass('show-sidebars processed').html('Show sidebars');
    $('#sidebar-first').hide();
    $('#sidebar-second').hide();
    $('#main').css({'width':'100%','left':0,'right':0});
  },function(){
    $(this).removeClass('show-sidebars').html('Hide sidebars');
    $('#sidebar-first').show();
    $('#sidebar-second').show();
    $('#main').removeAttr('style');
  });

}
