Drupal.behaviors.opengrid= function(context) {
//Theme Settings Widgets
  $('.hidden-settings').hide();
  $('.settings_trigger:checked').each(function(){
    $(this).parents('fieldset:first').find('.hidden-settings').show();
  });

  $('.settings_trigger:checkbox').change(function(){

    if($(this).is(':checked')) {
      $(this).parents('fieldset:first').find('.hidden-settings').show();
    } else {
      $(this).parents('fieldset:first').find('.hidden-settings').hide();
    }
  });

  $('.superfish-options input:radio').each(function(){
    if($(this).is(':checked')) {
      if($(this).val() == 0 || $(this).val() == 1) {
        $(this).parents('fieldset:first').find('.hidden-settings').show();
      }
    }
  });
  $('.superfish-options input:radio').change(function(){
    if($(this).is(':checked') && ($(this).val() == 0 || $(this).val() == 1)) {
      $(this).parents('fieldset:first').find('.hidden-settings').show();
    } else {
      $(this).parents('fieldset:first').find('.hidden-settings').hide();
    }
  });


//Tray Region
  $('#tray-wrapper .block h3.title:not(".tray-active-block")').click(function(){
    var $this = $(this);
    if(!$this.hasClass('tray-block-active')){
      $('#tray-wrapper h3.title').removeClass('tray-block-active');
      $this.toggleClass('tray-block-active');
      $(this).parent('.block').find('.content').css({'display':'block'});
      $('#tray-wrapper h3.title').each(function(){
  if(!$(this).hasClass('tray-block-active')){
    $(this).parent('.block').find('.content').hide();
  }
      });
    } else {
      $this.removeClass('tray-block-active').parent('.block').find('.content').hide();
    }
  });
  $('.tray-close:not("button-active")').toggle(function(){
    $(this).addClass('button-active');
    $('#tray-wrapper').slideDown('slow');
    $(this).css({'margin-top':'50px'});
  },function(){
    $(this).removeClass("button-active");
    $('#tray-wrapper').slideUp('slow');
  });


//Superfish Settings
  var superfish_settings = Drupal.settings.superfish_settings.superfish;
  if(superfish_settings) {

    $(".superfish ul.menu").supersubs({
            minWidth:    Drupal.settings.superfish_settings.sf_subs_min_width,   // minimum width of sub-menus in em units
            maxWidth:    Drupal.settings.superfish_settings.sf_subs_max_width,   // maximum width of sub-menus in em units
            extraWidth:  1     // extra width can ensure lines don't sometimes turn over
                               // due to slight rounding differences and font-family
        }).superfish({
        delay: Drupal.settings.superfish_settings.sf_delay,
        autoArrows : false,
        speed : Drupal.settings.superfish_settings.sf_speed,
        dropShadows: Drupal.settings.superfish_settings.sf_dropshadows,
        disableHI: Drupal.settings.superfish_settings.sf_hi,
    });
    if(Drupal.settings.superfish_settings.sf_subs_centralized) {
      $('li.expanded').addClass('centralized').append('<div class="sf-top-arrow" />');
      $('li.centralized').each(function(){
        sfLi_parent_width = $(this).width();
        sfLi_child_width = $(this).children('ul').width();
        sfUl_margin_left = (sfLi_parent_width - sfLi_child_width)/2;
        $(this).children('ul.menu:first').css({'margin-left': sfUl_margin_left +'px'});
      });
    }
  }

// Labelify Code
  forms_ids = '';

  if(typeof Drupal.settings.labelify_theme_settings != 'undefined'){
    forms_ids = Drupal.settings.labelify_theme_settings.labelify_forms;
  };

  if(forms_ids !== null) {
    $.each(forms_ids, function(key, value) {
        //find recursive solution to this
        key = key.replace('_','-');
        key = key.replace('_','-');

          if(key != 'node-form') {
            if($('#'+ key).find('.form-text').addClass('labelify').labelify) {
              $('#'+ key).find('.form-text').addClass('labelify').labelify({ text: "label"   });
            }
              $('#'+ key).find('.form-text').parent('.form-item').addClass('form-item-labelify');
              $('#'+ key).find('.form-text').parent('.form-item').find('label').hide();
              $('#'+ key).find('.form-text').parent('.form-item').find('.label-wrapper').hide();

          } else {
            if($('.node-add-section #'+ key).find('#edit-title').addClass('labelify').labelify) {
              $('.node-add-section #'+ key).find('#edit-title').addClass('labelify').labelify({ text: "label"   });
            }
              $('#'+ key).find('#edit-title').addClass('form-item-labelify');
              $('.node-add-section #'+ key).find('#edit-title').parent('.form-item').find('label').hide();
              $('.node-add-section #'+ key).find('#edit-title').parent('.form-item').find('.label-wrapper').hide();

          }
    });
  }

/*
* Adds a Background for Browse Buttons for inputs of the type file.
*/
  var fakefile = '<div class="fakefile"><input class="fakeinput" type="text" /><span class="browse-button" >'+ Drupal.t('Browse') +'</span></div>';

  $('input:file:not(region-processed)',context).each(function(){
    $(this).wrap('<div class="file-wrapper" />');
    $(this).addClass('hidden').parents('.form-item:first').find('.file-wrapper').css({'position' : 'relative'}).append(fakefile);

    $(this).change(function(){
        $(this).parents('.form-item:first').find('.fakefile input.fakeinput').val($(this).val());
    });

  });

  $('input:file').mouseover(function(event){
    $(this).parents('.form-item:first').find('.browse-button').addClass('button-hover');
  }).mouseout(function(){
    $(this).parents('.form-item:first').find('.browse-button').removeClass('button-hover');
  });
}
