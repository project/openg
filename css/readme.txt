CSS Code Formatting Guide for OpenG

Separate style sheets according to function.
In OpenG we have
- og.layout.css: all layout related rules
- og.reset.css: resets default HTML elements
- og.core.css: default HTML and Drupal related rules
- og.typography.css: for yea with typography fetishes
- og.aesthetics.css: ui customizations (borders, colors, bgs etc.)
- og.extras.css: module specific customizations
- og.forms.css: forms
- superfish.xxx : superfish menu style

Drupal optionally aggregates style sheets, so lets make use of this and
separate our style for the sake of better work-flow and organization.

Typography
body font size is set to 62.5% meaning that 1em = 10px. This allows us
to have a better work-flow when translating design from graphic programs like Inkscape
to CSS. 1.1em is 11px, 1.2em is 12px and so on. As such, after the initial
rule using percentage, further font sizes are controlled by em and almost never by pixels.

Padding and margin
Where a fixed position or size is required, use pixels (px). Otherwise use em.

Comment and use line-breaks for more readable code. Drupal's aggregator
will optimize the files to the extent of removing comments. Optionally
files can be merged and externally optimized when the site goes into
production.

Below are some general guidelines on how to format code. Well formatted code
is easy to read and develop on. Be good. Play nice.

- for multiple selector rules place each selector in new line
- Use indentation for related selectors
- Comment CSS3 declarations and place at end of selector rule-set
- space before opening selector bracket and on same line as selector
- line-break for closing bracket
- arrange rules alphabetically within each selector
- line-break to separate each unrelated selector class
- keep inline comments short else move above related rule/class
- if any of this does not suit you, do it your way


Sample code block

/*****************************************************************
****************   Section
****************************************************************/

/** START -simpletabs *************/

.simpleTabs .primary-links ul.links,
.simpleTabs ul.tabs {
  background:none;
  border-bottom:1px #000 solid;
  padding-left:2px;/* LTR */
  }

/** END -simpletabs **************/

/** START -emac ************/

.emac ul.links li,
.emac ul.links li a {
  margin:0;
  padding:0;
  }
  .emac ul.links li a {
    background:#333;
    color:#fff;
    font-weight:bold;
    padding:.5em 1em;
    /*CSS3*/
    text-shadow: #e00 1px 1px 0;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    }

/** END -emac *************************/
