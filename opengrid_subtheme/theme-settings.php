<?php
// $Id$

// Include the definition of opengrid_settings, opengrid_initialize_theme_settings() and opengrid_default_theme_settings().
include_once './' . drupal_get_path('theme', 'opengrid') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function opengrid_subtheme_settings($saved_settings) {

  global $base_url;
  
  //print dsm($saved_settings);

  // Get theme name from url (admin/.../theme_name)
  $theme_name = arg(count(arg()) - 1);

  // Combine default theme settings from .info file & theme-settings.php
  $theme_data = list_themes();   // get data for all themes
  $info_theme_settings = ($theme_name) ? $theme_data[$theme_name]->info['settings'] : array();
  $defaults = array_merge(opengrid_default_theme_settings(), $info_theme_settings);
  
  // Combine default and saved theme settings
  $settings = array_merge($defaults, $saved_settings);
  
  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();

  // Add the base theme's settings.
  $form += opengrid_settings($saved_settings, $defaults);

  return $form;
}
