Opengrid Subtheme Implementation:

A Subtheme is a theme that inherits the characteristics of its parent theme. This is very convenient especially in multi-site installations. For more information read <a href="http://drupal.org/node/225125">D.O handbook on subthemes.</a>

Steps to Implement Subthemes with Opengrid.

Copy opengrid_subtheme folder and paste it inside the sites/all/themes folder, or within my_site.com/themes folder if this is a multisite installation type.


Normally we want to change the name of the subtheme to one more related to our project. The changes we should make would be:

   1. Rename the folder opengrid_subtheme by the name we have chosen: Examples: Aquarius
   2. Rename the Info file:
      .info file:opengrid_subtheme.info -> aquarius.info
   3. Inside the info file:line 8: name = Open Grid subtheme -> name = Aquarius
   4. Within the file of theme_settings.php line 16: function opengrid_subtheme_settings ($ saved_settings) -> aquarius_settings ($ saved_settings)
   5. From the page /admin/build/themes select you should see our new theme Aquarius listed among the other themes.


Custom CSS.

It is recommended to create a new CSS file in which to write the specific style for our new theme. This is done from the .info file of our subtheme. Example:

line 26 stylesheets [all] [] = css/aquarius.css

Note:

Within the .info file lines preceded by a ";" implies that this file is commented. If we over-write it, remove the ";".
