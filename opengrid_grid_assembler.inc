<?php

function opengrid_grid_settings_vars($vars) {
  $theme = variable_get('theme_default','none');
  $settings = theme_get_settings($theme);

  //customizing body classes
  $vars['body_classes'] = ($vars['sidebar_first']) ? str_replace('sidebar-left', 'first-sidebar',$vars['body_classes']) : $vars['body_classes'];
  $vars['body_classes'] = ($vars['sidebar_second']) ? str_replace('sidebar-right', 'second-sidebar',$vars['body_classes']) : $vars['body_classes'];

  //setting layout value
  $layout = 'no_sidebars_layout';
  $layout = ($vars['sidebar_first']) ? 'sidebar_first_layout' : $layout;
  $layout = ($vars['sidebar_second']) ? 'sidebar_second_layout' : $layout;
  $layout = ($vars['sidebar_first'] && $vars['sidebar_second']) ? 'both_sidebars_layout' : $layout;
  if (theme_get_setting('frontpage_layout_checkbox') == 1 && drupal_is_front_page()) {
    $layout =  'frontpage_layout';
  }

  $new_vars =  layout_grid_assembler($layout,$settings);

  if (is_array($new_vars)) {
    $vars = array_merge($vars, $new_vars);
  }

  return $vars;
}

function layout_grid_assembler($layout,$settings) {
  $class = array();

  //load values for containers
  if ($settings['container_columns'] != 'none' &&  $settings['independent_containers'] != 1) {
    $class['page_classes'][] =  $settings['container_columns'];
  }
  if ($settings['container_extras'] != '') {
    $class['page_classes'][] = $settings['container_extras'];
  }
  //main navigation classes
  if (theme_get_setting('dropdown_options') == 0 || theme_get_setting('dropdown_options') == 1) {
    $class['main_navigation_classes'][] =  ' superfish';
  }
  if ($settings['navigation_extras']) {
    $class['main_navigation_classes'][] .=  ' '. $settings['navigation_extras'];
  }
  //if containers are independent add the class container to the main navigation div
  if ($settings['container_columns'] != 'none' && $settings['independent_containers'] == 1) {
    $class['main_navigation_classes'][] =  ' '. $settings['container_columns'];
  }


  //load values for grids
  if (is_array($settings[$layout]['grids_settings'])) {
    $regions = array('grid', 'prefix', 'suffix', 'push', 'pull');
    foreach ($settings[$layout]['grids_settings'] as $key => $values) {
      foreach ($regions as $data) {
        $class[$key][] = $values[$data] != 0 ?  $data .'_'. $values[$data] : '';
      }
      $class[$key][] = ($values['extras'] != '') ? $values['extras'] : '';
      $class[$key][] = ($values['alpha'] == 1) ? 'alpha' : '';
      $class[$key][] = ($values['omega'] == 1) ? 'omega' : '';
    }
  }
  if ($settings['independent_containers'] == 1) {
    $containers = array(
      'header_classes' => t('Header container'),
      'content_top_classes' => t('Content Top container'),
      'content_classes' => t('Content container'),
      'content_bottom_classes' => t('Content Bottom container'),
      'footer_classes' => t('Footer container'),
    );
    foreach ($containers as $container_key => $name) {
        //$container_key = str_replace('_grid','_classes', $key);
        $class[$container_key][] = ($settings['container_columns'] != 'none' && $settings[$container_key] != 'none') ? $settings['container_columns'] : '';
        $class[$container_key][] = ($settings[$container_key] != 'none') ? $settings[$container_key] : '';
    }
  }
  if (is_array($class)) {
    foreach ($class as $key => $classes) {
      $vars[$key] = implode(' ', $class[$key]);
    }
  }

  return $vars;
}
